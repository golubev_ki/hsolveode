module Lib where

-- import Debug.Trace hiding (trace)
import Prelude hiding ((<>))
import Debug.Trace
import Numeric.LinearAlgebra.HMatrix 
dtrace _ y = y 

-- trace _ y = y

-- WARNING the 550 points is the limit. Further theta is gonna become Nan

tridiagonalInverse :: Int -> (Double, Double, Double) -> Matrix Double

tridiagonalInverse n (c, a, b) =
    let rowOrderTridiag n =
            let generator idx 
                    | idx == n*n = []
                    | i - j == 0 = a : next
                    | i - j == 1 = c : next
                    | i - j == -1 = b : next
                    | otherwise = 0.0 : next
                    where 
                        (i, j) = (idx `quot` n, idx `rem` n)
                        next = generator (idx + 1)
            in generator 0
        start = matrix n (rowOrderTridiag n)
        inversed = inv start
    in inversed


data CalcField = CalcField {step' :: Double, xs' :: [Double]} deriving Show
data BorderCond = BorderCond {
    getg0  :: Double -> Double,
    getg0' :: Double -> Double,
    getg1  :: Double -> Double,
    getg1' :: Double -> Double,
    getu0 :: Double -> Double,
    getf :: Double -> Double -> Double}

generateCalcField :: Int -> CalcField 

generateCalcField n = 
    let h = (1.0 :: Double) / ((fromIntegral n :: Double) - (1.0 :: Double))
        generator n 
            | n == 0 = [0.0]
            | otherwise = generator (n - 1) ++ [(fromIntegral n :: Double) * h]
    in CalcField h $ generator (n - 1)

generateBasis :: CalcField -> (Double -> Double) -> [Double -> Double]
generateBasis field bf = 
    let generator [] = []
        generator (xj:xs) =  (\x -> bf ((x - xj)/(step' field))) : generator xs
    in generator (xs' field)


makeTridiagonal :: Int -> (Double, Double, Double) -> Matrix Double
makeTridiagonal n (c, a, b) =
    let rowOrderTridiag n =
            let generator idx 
                    | idx == n*n = []
                    | i - j == 0 = a : next
                    | i - j == 1 = c : next
                    | i - j == -1 = b : next
                    | otherwise = 0.0 : next
                    where 
                        (i, j) = (idx `quot` n, idx `rem` n)
                        next = generator (idx + 1)
            in generator 0
    in matrix n (rowOrderTridiag n)



genT :: Int -> Matrix Double
genT n = makeTridiagonal n ((negate 1.0), 2.0, (negate 1.0))

genS :: Int -> Matrix Double
genS n = makeTridiagonal n (1.0, 4.0, 1.0)



genfvector :: CalcField -> 
            (Double -> Double -> Double) -> -- f(x, t) as in left part of initial equation
            [Double -> Double] -- list of functons which represents f(t) : t -> Vector

genfvector field f@fFromEq = 
    let generator :: [Double] -> [Double -> Double]
        generator [ _ ] = [] -- ignore last element
        generator (x: xs) = 
            let fx = (\t -> f t x)
            in fx : generator xs
    in generator . tail . xs' $ field -- ran on everything execpt the first element


genfaddition :: CalcField -> BorderCond ->
        [Double -> Double] -- list of functons which represents f(t) : t -> Vector

genfaddition field borderCond = 
    let generator :: [Double] -> [Double -> Double]
        generator [] = []
        generator (x : xs) = (\t -> zerofiller t) : generator xs -- fill the rest with zeros
        zerofiller :: Double -> Double
        zerofiller _ = 0.0
        g0 = getg0 borderCond
        g1 = getg1 borderCond
        g0' = getg0' borderCond
        g1' = getg1' borderCond
        f = getf borderCond
        h = step' field
        xs = xs' $ field
        fFist = \t -> (h / 6.0 * ( (f t $ head xs) -
                                                (g0' t) + 
                                                6.0 / (h**2.0) * (g0 t) 
                                ))
        fLast = \t -> (h / 6.0 * ( (f t $ head xs) -
                                                (g1' t) + 
                                                6.0 / (h**2.0) * (g1 t)
                                ))
        result = [\t -> fFist t] ++  (generator . drop 4 $ xs) ++ [\t -> fLast t]
    in result -- ran on every element except the last


listF2Flist :: [Double -> Double] -> (Double -> [Double])

listF2Flist fs = 
    let generator _ [] = []
        generator t (f:fs) = (f t) : generator t fs
    in (\t -> generator t fs)





genGenF :: Int -> BorderCond -> (Double -> Vector Double, Int)

genGenF n borderCond = 
    let field = generateCalcField n 
        f = getf borderCond
        h = step' field
        fistPartFunc = listF2Flist (genfvector field f)
        secondPartFunc = listF2Flist (genfaddition field borderCond)
        n' = length (genfvector field f)
        bigS = genS n'
    in  (\t -> (h / 6.0) `scale`
                        bigS #> (fromList $ fistPartFunc $ t) +
                                (fromList $ secondPartFunc $ t)
        ,n')

genu' :: Int -> BorderCond -> (Double -> Vector Double -> Vector Double)
genu' n borderCond = 
    let (genF, n') = genGenF n borderCond
        field = generateCalcField n
        h = step' field
        bigA = dtrace ("computing A") (1.0 / h) `scale` (genT n')
        -- bigD = dtrace ("computing D") (h / 6.0) .* (genS n')
        invD = (6.0 /h) `scale` (tridiagonalInverse n' (1.0, 4.0, 1.0))
        productInvDA = dtrace ("computing D^-1 times A") (invD <> bigA)
    in      (\t x -> 
                    let first = invD #> (genF t)
                        second = invD #> (bigA #> x)
                        result = first - second
                    in result
             ) 


genSimpu' :: Int -> BorderCond -> (Double -> Vector Double -> Vector Double)
genSimpu' n borderCond = 
    let (genF, n') = genSimpGenF n borderCond
        field = generateCalcField n
        h = step' field
        bigA = dtrace ("computing A") (1.0 / h**2.0) `scale` (genT n')       
    in      (\t x -> 
                    let first = (genF t)
                        second = bigA #> x
                        result = first - second
                    in result
             ) 



genSimpGenF :: Int -> BorderCond -> (Double -> Vector Double, Int)
genSimpGenF n borderCond = 
    let field = generateCalcField n 
        f = getf borderCond
        h = step' field
        fistPartFunc = listF2Flist (genfvector field f)
        secondPartFunc = listF2Flist (genSimpfaddition field borderCond)
        n' = length (genfvector field f)
        bigS = genS n'
    in  (\t -> (fromList $ fistPartFunc $ t) +
                    (fromList $ secondPartFunc $ t)
        ,n')



genSimpfaddition :: CalcField -> BorderCond ->
        [Double -> Double] -- list of functons which represents f(t) : t -> Vector

genSimpfaddition field borderCond = 
    let generator :: [Double] -> [Double -> Double]
        generator [] = []
        generator (x : xs) = (\t -> zerofiller t) : generator xs -- fill the rest with zeros
        zerofiller :: Double -> Double
        zerofiller _ = 0.0
        g0 = getg0 borderCond
        g1 = getg1 borderCond
        f = getf borderCond
        h = step' field
        xs = xs' $ field
        fFist = \t -> 1.0 / h**2.0 * (g0 t)
        fLast = \t -> 1.0 / h**2.0 * (g1 t)
        result = [\t -> fFist t] ++  (generator . drop 4 $ xs) ++ [\t -> fLast t]
    in dtrace ("length: " ++ (show . length $ result)) result -- ran on every element except the last