{-# LANGUAGE ExtendedDefaultRules #-}


import Lib
import Numeric.GSL.ODE
import Numeric.LinearAlgebra.HMatrix
import Graphics.Matplotlib
import Debug.Trace

n::Int
n = 15

hi = 10.0**(negate(4.0))
epsAbs = 1.49012e-08
epsRel = epsAbs

borderCond = BorderCond 
    (\_ -> 0.0) -- g0
    (\_ -> 0.0) -- g0'
    (\_ -> 1.0) -- g1
    (\_ -> 0.0) -- g1'
    (\x -> (1 + negate(x))) -- u0 
    -- (\t x -> case () of  
    --                 _ | abs (x - 0.5) < 0.05 -> 100.0
    --                   | otherwise -> 0.0
    -- ) -- f
    -- (\t x -> 10.0**2.0*(1 + negate(x)))
    (\t x -> 0.0)

field = generateCalcField n
xs0 = tail . init . xs' $ field
ts = linspace 100 (0, 0.2 :: Double)


norm :: Vector Double -> Double
norm vec = sqrt $ foldl (\x acc -> x*x + acc) 0.0 (toList vec) 



solve :: (Double -> Vector Double -> Vector Double) -> Matrix Double
solve u' =
    let us0 = fromList $ fmap (getu0 borderCond) xs0
        sol = odeSolveV RKf45 hi epsAbs epsRel u' us0 ts
    in sol


uSimp' = genSimpu' n borderCond

u' = genu' n borderCond

solSimp = trace ("comuting lm:") $ solve uSimp'

sol = trace ("computing fem:") $ solve u'

pickEvery n xs = 
    let generator [] = []
        generator (x:xs) = x : (generator . drop (n - 1) $ xs)
    in generator xs


plotingFunc axes values pltFunc = 
    let plotLines [vector] = pltFunc axes vector
        plotLines (vector : others) = (pltFunc axes vector) % (plotLines others)
    in plotLines values

diffsol = sol - solSimp

femplotData = fmap toList $ toRows sol
lmplotData = fmap toList $ toRows solSimp

plotDeviation = [fmap norm $ toRows diffsol]

-- mlegend = plotMapLinear (\x -> x ** 2) 0 1 100 @@ [o2 "label" "x^2"]
--   % plotMapLinear (\x -> x ** 3) 0 1 100 @@ [o2 "label" "x^3"]
--   % legend @@ [o2 "fancybox" True, o2 "shadow" True, o2 "title" "Legend", o2 "loc" "upper left"]

-- 
myThen x y = y

main = do 
    file "fem.png" $ (plotingFunc xs0 femplotData plot)
    file "lm.png" $ (plotingFunc xs0 lmplotData plot)
    file "error.png" $ (plotingFunc (fromList [1..(length . toList $ ts)]) plotDeviation plot)
